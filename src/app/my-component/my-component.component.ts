import { PokemonSharInfoService } from './../pokemon-shar-info.service';
import { PokeAPIServiceService } from './../poke-apiservice.service';
import { Pokemon, PokemonDetail } from './../pokemon';
import { Component, OnInit } from '@angular/core';
import { Element } from '@angular/compiler';

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css'],
  providers: [PokeAPIServiceService, PokemonSharInfoService]
})
export class MyComponentComponent implements OnInit {

  id: string;
  selectedPokemonID: string;
  searchPokemonName = '';

  myDate : Date;

  tabPokemon: Pokemon[] = [];
  pokemonDetails: PokemonDetail;

  option = document.querySelector("select");

  constructor(private pokemonService: PokeAPIServiceService,
    private pokemonSharInfoService: PokemonSharInfoService) {

  }

  ngOnInit(): void {
    this.pokemonService.getPokemons()
      .subscribe(data => {
        data.results.forEach((element, index) => {
          var indexPokeReel = index+1;
          this.tabPokemon.push(new Pokemon('' + indexPokeReel, element.name, element.url));
        });
      });
  }

  go() {
    console.log(this.selectedPokemonID);
    if (this.selectedPokemonID != '') {
      this.pokemonService.getPokemonInfo(this.selectedPokemonID)
        .subscribe(data => {
          this.pokemonDetails = data;
          this.pokemonSharInfoService.setValue(this.selectedPokemonID);
        });
    }
  }
  /*this.pokemonSharInfoService.setValue(this.selectedPokemonID);
  if (this.selectedPokemonID != '') {
    this.pokemonService.getPokemonInfo(this.selectedPokemonID).subscribe;
  }*/
}

