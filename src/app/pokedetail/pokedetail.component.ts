import { Component, OnInit, Input } from '@angular/core';
import { PokemonDetail } from '../pokemon';
import { PokemonSharInfoService } from './../pokemon-shar-info.service';

@Component({
  selector: 'app-pokedetail',
  templateUrl: './pokedetail.component.html',
  styleUrls: ['./pokedetail.component.css'],
  providers: []
})
export class PokedetailComponent implements OnInit {

  @Input('detail')
  detail: PokemonDetail;

  constructor(private pokemonSharInfoService: PokemonSharInfoService) {
    this.pokemonSharInfoService.getObservable().subscribe(e => console.log('e : ' + e));
  }

  ngOnInit(): void {
    //console.log(this.pokemonSharInfoService.getValue());
  }

}
