import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonSharInfoService {

  //value:String;

  public stringVar = new Subject<string>();

  //constructor() { }

  getObservable(): Subject<string>{
    return this.stringVar;
  }

  public setValue(newStringVar:string){
    this.stringVar.next(newStringVar);
  }

  /*setValue(value:String){
    this.value = value;
  }*/

  /*getValue() : String{
    return this.value
  }*/
}
