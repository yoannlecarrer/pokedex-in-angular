import { TestBed } from '@angular/core/testing';

import { PokemonSharInfoService } from './pokemon-shar-info.service';

describe('PokemonSharInfoService', () => {
  let service: PokemonSharInfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonSharInfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
